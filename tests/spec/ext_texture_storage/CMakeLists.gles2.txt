link_libraries (
	piglitutil_${piglit_target_api}
)

piglit_add_executable (ext_texture_storage-formats formats.c)

# vim: ft=cmake:
